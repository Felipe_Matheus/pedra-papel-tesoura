const aleatorio = (min, max) => {
  let cont = Math.random() * (max - min) + min;
  return Math.round(cont);
};
let mao_computador = aleatorio(0, 15);
let mao_player = "none";
const expressao = document.getElementById("imagem");
const pedradiv = document.getElementById("pedra");
const papeldiv = document.getElementById("papel");
const tesouradiv = document.getElementById("tesoura");
//fiz a mão do pc
let conteudo = document.createElement("p");
const disputa = (robo, humano) => {
  if (robo === humano) {
    conteudo.innerHTML = `Maquina: ${robo}- Empate - Player: ${humano}`;
    atual.appendChild(conteudo);
    expressao.style.backgroundPositionX = "-259px";
  } else if (
    (robo === "tesoura" && humano === "papel") ||
    (robo === "papel" && humano === "pedra") ||
    (robo === "pedra" && humano === "tesoura")
  ) {
    conteudo.innerHTML = `Maquina: ${robo}- Perdeu - Player: ${humano}`;
    atual.appendChild(conteudo);
    expressao.style.backgroundPositionX = "-112px";
  } else {
    conteudo.innerHTML = `Maquina: ${robo}- Ganhou - Player: ${humano}`;
    atual.appendChild(conteudo);
    expressao.style.backgroundPositionX = "-4px";
  }
};
//aqui vai ser a função para as condições de competição
const atual = document.getElementById("mao");
const geraratual = (element, posicao) => {
  element = posicao;
  mao_computador = aleatorio(0, 15);
  if (mao_computador >= 0 && mao_computador <= 5) {
    mao_computador = "pedra";
  } else if (mao_computador >= 5 && mao_computador <= 10) {
    mao_computador = "tesoura";
  } else {
    mao_computador = "papel";
  }
  //----------------------------------------//
  disputa(mao_computador, element);
  return element;
};
pedradiv.addEventListener("click", () => {
  geraratual(mao_player, "pedra");
});
tesouradiv.addEventListener("click", () => {
  geraratual(mao_player, "tesoura");
});
papeldiv.addEventListener("click", () => {
  geraratual(mao_player, "papel");
});
